﻿using System;
using System.Windows.Forms;
using System.Drawing;
using SharpShell.SharpContextMenu;
using SharpShell.Attributes;
using System.IO.Pipes;
using System.IO;
using System.Text;

namespace hupload{

    [COMServerAssociation(AssociationType.AllFiles)]
    public class HUMenuExtension : SharpContextMenu{

        protected override bool CanShowMenu(){
            return true;
        }

        protected override ContextMenuStrip CreateMenu(){
            var menu = new ContextMenuStrip();

            var itemShare = new ToolStripMenuItem { Text = "Share" };
            itemShare.Click += (sender, args) => Share();
            menu.Items.Add(itemShare);

            return menu;
        }

        private void Share() {
            foreach(var item in SelectedItemPaths) Send(item);
        }

        public void Send(string msg) {
            var stream = new NamedPipeClientStream(".", "HUPipeServer", PipeDirection.Out, PipeOptions.Asynchronous);
            try { stream.Connect(250); } 
            catch (TimeoutException) { return; }
            byte[] buffer = Encoding.UTF8.GetBytes(msg);
            stream.BeginWrite(buffer, 0, buffer.Length, new AsyncCallback(onSend), stream);
        }

        private void onSend(IAsyncResult result) {
            var stream = (NamedPipeClientStream)result.AsyncState;
            stream.EndWrite(result);
            stream.Flush();
            stream.Close();
            stream.Dispose();
        }

    }
}
