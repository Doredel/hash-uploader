﻿namespace hupload {
    partial class ConfigForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.autolaunch = new System.Windows.Forms.CheckBox();
            this.httpPath = new System.Windows.Forms.TextBox();
            this.httpPathLabel = new System.Windows.Forms.Label();
            this.serverPath = new System.Windows.Forms.TextBox();
            this.serverPathLabel = new System.Windows.Forms.Label();
            this.username = new System.Windows.Forms.TextBox();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.server = new System.Windows.Forms.TextBox();
            this.port = new System.Windows.Forms.TextBox();
            this.serverLabel = new System.Windows.Forms.Label();
            this.portLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // autolaunch
            // 
            this.autolaunch.AutoSize = true;
            this.autolaunch.Location = new System.Drawing.Point(12, 143);
            this.autolaunch.Name = "autolaunch";
            this.autolaunch.Size = new System.Drawing.Size(223, 17);
            this.autolaunch.TabIndex = 6;
            this.autolaunch.Text = "Launch automatically on Windows startup";
            this.autolaunch.UseVisualStyleBackColor = true;
            // 
            // httpPath
            // 
            this.httpPath.Location = new System.Drawing.Point(12, 92);
            this.httpPath.Name = "httpPath";
            this.httpPath.Size = new System.Drawing.Size(190, 20);
            this.httpPath.TabIndex = 4;
            // 
            // httpPathLabel
            // 
            this.httpPathLabel.AutoSize = true;
            this.httpPathLabel.Location = new System.Drawing.Point(210, 95);
            this.httpPathLabel.Name = "httpPathLabel";
            this.httpPathLabel.Size = new System.Drawing.Size(60, 13);
            this.httpPathLabel.TabIndex = 2;
            this.httpPathLabel.Text = "HTTP path";
            // 
            // serverPath
            // 
            this.serverPath.Location = new System.Drawing.Point(12, 117);
            this.serverPath.Name = "serverPath";
            this.serverPath.Size = new System.Drawing.Size(190, 20);
            this.serverPath.TabIndex = 5;
            // 
            // serverPathLabel
            // 
            this.serverPathLabel.AutoSize = true;
            this.serverPathLabel.Location = new System.Drawing.Point(210, 120);
            this.serverPathLabel.Name = "serverPathLabel";
            this.serverPathLabel.Size = new System.Drawing.Size(62, 13);
            this.serverPathLabel.TabIndex = 4;
            this.serverPathLabel.Text = "Server path";
            // 
            // username
            // 
            this.username.Location = new System.Drawing.Point(12, 66);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(190, 20);
            this.username.TabIndex = 3;
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Location = new System.Drawing.Point(210, 69);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(55, 13);
            this.usernameLabel.TabIndex = 6;
            this.usernameLabel.Text = "Username";
            // 
            // server
            // 
            this.server.Location = new System.Drawing.Point(12, 13);
            this.server.Name = "server";
            this.server.Size = new System.Drawing.Size(190, 20);
            this.server.TabIndex = 1;
            // 
            // port
            // 
            this.port.Location = new System.Drawing.Point(12, 40);
            this.port.Name = "port";
            this.port.Size = new System.Drawing.Size(190, 20);
            this.port.TabIndex = 2;
            // 
            // serverLabel
            // 
            this.serverLabel.AutoSize = true;
            this.serverLabel.Location = new System.Drawing.Point(210, 16);
            this.serverLabel.Name = "serverLabel";
            this.serverLabel.Size = new System.Drawing.Size(38, 13);
            this.serverLabel.TabIndex = 9;
            this.serverLabel.Text = "Server";
            // 
            // portLabel
            // 
            this.portLabel.AutoSize = true;
            this.portLabel.Location = new System.Drawing.Point(210, 43);
            this.portLabel.Name = "portLabel";
            this.portLabel.Size = new System.Drawing.Size(26, 13);
            this.portLabel.TabIndex = 10;
            this.portLabel.Text = "Port";
            // 
            // ConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 166);
            this.Controls.Add(this.portLabel);
            this.Controls.Add(this.serverLabel);
            this.Controls.Add(this.port);
            this.Controls.Add(this.server);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.username);
            this.Controls.Add(this.serverPathLabel);
            this.Controls.Add(this.serverPath);
            this.Controls.Add(this.httpPathLabel);
            this.Controls.Add(this.httpPath);
            this.Controls.Add(this.autolaunch);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ConfigForm";
            this.Text = "Config";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConfigForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ConfigForm_FormClosed);
            this.Load += new System.EventHandler(this.ConfigForm_Load);
            this.Shown += new System.EventHandler(this.ConfigForm_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox autolaunch;
        private System.Windows.Forms.TextBox httpPath;
        private System.Windows.Forms.Label httpPathLabel;
        private System.Windows.Forms.TextBox serverPath;
        private System.Windows.Forms.Label serverPathLabel;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.TextBox server;
        private System.Windows.Forms.TextBox port;
        private System.Windows.Forms.Label serverLabel;
        private System.Windows.Forms.Label portLabel;
    }
}