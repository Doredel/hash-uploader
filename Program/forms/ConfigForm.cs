﻿using System.Windows.Forms;
using System;

namespace hupload {
    public partial class ConfigForm : Form {
        
        public ConfigForm() : base() {
            InitializeComponent();
        }

        private void ConfigForm_FormClosing(object sender, FormClosingEventArgs e) {

        }

        private void ConfigForm_FormClosed(object sender, FormClosedEventArgs e) {
            Config.server = server.Text;
            Config.port = Convert.ToInt32(port.Text);

            Config.httpPath = httpPath.Text;
            Config.username = username.Text;
            Config.serverPath = serverPath.Text;
            Config.autolaunch = autolaunch.Checked;

            Config.Save();

            if (!Config.CheckValid()) {
                //Program.mainForm.DisableConnect();
                //return;
            }
            Program.mainForm.DemandConnect();
        }

        private void ConfigForm_Load(object sender, EventArgs e) {

        }

        private void ConfigForm_Shown(object sender, EventArgs e) {
            server.Text = Config.server;
            port.Text = Config.port.ToString();

            httpPath.Text = Config.httpPath;
            username.Text = Config.username;
            serverPath.Text = Config.serverPath;
            autolaunch.Checked = Config.autolaunch;
        }

    }

    
}
