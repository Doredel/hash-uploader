﻿using System;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace hupload
{
    public partial class ConnectionForm : Form{

        private NotifyIcon appIcon;

        ConfigForm configForm;

        public ConnectionForm(){
            Icon = Properties.Resources.AppIcon;
            appIcon = new NotifyIcon();
            appIcon.Icon = Properties.Resources.AppIcon;
            appIcon.MouseClick += new MouseEventHandler(AppIcon_MouseClick);
            InitializeComponent();
            FilesCountStatus.Text = "";
        }

        private void ConnectionForm_FormClosing(object sender, EventArgs e) {
            Program.DisconnectFromServer();
        }

        public void DemandConnect() {
            this.ActiveControl = passwordBox;
            passwordBox.Focus();
            connectButton.Enabled = true;
            passwordBox.Enabled = true;
        }

        public void AfterConnect() {
            ToggleConnectionButton(false);
            configButton.Enabled = false;
            passwordBox.Enabled = false;
            Pipe.StartReading();
        }

        public void AfterDisconnect() {
            ToggleConnectionButton(true);
            configButton.Enabled = true;
            passwordBox.Enabled = true;
            Pipe.StopReading();
        }

        public void UpdateFileCount(int count) {
            if (InvokeRequired) {
                this.Invoke(new Action<int>(UpdateFileCount),count);
                return;
            }
            FilesCountStatus.Text = "Files: " + count;
        }

        public void ToggleConnectionButton(bool mode) {
            if (mode) {
                connectButton.Text = "Connect";
                connectButton.Click -= disconnectButton_Click;
                connectButton.Click += connectButton_Click;
            } else {
                connectButton.Text = "Disconnect";
                connectButton.Click -= connectButton_Click;
                connectButton.Click += disconnectButton_Click;
            }
        }

        private void connectButton_Click(object sender, EventArgs e) {
            if (Program.ConnectToServer(passwordBox.Text)) {
                Program.Log("Server connected");
                AfterConnect();
                this.WindowState = FormWindowState.Minimized;
            }
        }

        private void disconnectButton_Click(object sender, EventArgs e) {
            Program.DisconnectFromServer();
            Program.Log("Server disconnected");
            AfterDisconnect();
        }

        private void passwordBox_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) connectButton_Click(this, new EventArgs());
        }

        public void AddLogMsg(string msg) {
            if (InvokeRequired) {
                this.Invoke(new Action<string>(AddLogMsg), new object[] { msg });
                return;
            }
            logBox.Text += msg + "\r\n";
        }

        private void ConnectionForm_Resize(object sender, EventArgs e) {
            if (this.WindowState == FormWindowState.Minimized) {
                appIcon.Visible = true;
                this.Hide();
            } else if (this.WindowState == FormWindowState.Normal) {
                appIcon.Visible = false;
            }

        }

        public void ShowWindow(){
            if (InvokeRequired) {
                this.Invoke(new Action(ShowWindow));
                return;
            }
            Show();
            WindowState = FormWindowState.Normal;            
        }

        private void AppIcon_MouseClick(object sender, MouseEventArgs e) {
            ShowWindow();
        }

        private void ConnectionForm_Shown(object sender, EventArgs e) {
            configForm = new ConfigForm();
            if (!Config.CheckValid()) configForm.ShowDialog();
        }

        private void configButton_Click(object sender, EventArgs e) {
            configForm.ShowDialog();
        }

    }
}
