﻿using System;
using System.IO;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Globalization;
using System.Windows;

namespace hupload{
    
    static class Program
    {
        static Server server;
        public static ConnectionForm mainForm { get; private set; }

        const string registryPath = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";
        const string appRegistryName = "HUploader";

        /// The main entry point for the application.
        static void Main(){
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var log = new FileStream("./log.txt", FileMode.Create, FileAccess.Write);
            var writer = new StreamWriter(log) { AutoFlush = true };

            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;

            Console.SetOut(writer);
            Console.SetError(writer);

            mainForm = new ConnectionForm();

            Application.Run(mainForm);
        }

        static public bool ConnectToServer(string password) {
            server = new Server(password);
            if (!server.Connect()) return false;
            return true;
        }

        static public void DisconnectFromServer() {
            server.Disconnect();
        }

        static public void Submit(string filename){
            var task = new UploadingTask(filename, server);
            try {
                task.Run();
            } catch (Exception ex) {
                string errtext = string.Format("Exception occurred while running task for file {0}", filename);
                LogError(ex,errtext);
                mainForm.ShowWindow();
            }
        }

        static public void Log(string message) {
            mainForm.AddLogMsg(message);
        }

        static public void LogError(Exception ex){
            Console.WriteLine(ex.ToString());
            Log(ex.Message);
        }

        static public void LogError(Exception ex, string desc){
            Log(desc);
            Console.WriteLine(desc);
            LogError(ex);
        }

        static public void RegisterAutoLaunch() {
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(registryPath,true);
            registryKey.SetValue(appRegistryName, Application.ExecutablePath);
        }

        static public void UnregisterAutoLaunch() {
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(registryPath,true);
            registryKey.DeleteValue(appRegistryName);
        }

        /*static private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e) {

        }*/

    }
    
}