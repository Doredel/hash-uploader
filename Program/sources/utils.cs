﻿using System.IO;
using System.Text;
using System;
using System.Collections.Generic;

namespace hupload{

    public static class Utils{

        public static string calcHash(string filename){
            
            byte[] md5hash;

            using (var md5 = System.Security.Cryptography.MD5.Create()) {
                using (var stream = File.OpenRead(filename)){
                    md5hash = md5.ComputeHash(stream);
                }
            }

            StringBuilder sb = new StringBuilder();

            foreach (byte b in md5hash){
                sb.Append(b.ToString("x2"));
            }

            return sb.ToString();
        }

        public static TValue GetValueOrDefault<TKey,TValue>(this IDictionary<TKey, TValue> dict, TKey key) {
            TValue val;
            return dict.TryGetValue(key, out val) ? val : default(TValue);
        }

    }
}