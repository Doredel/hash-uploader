namespace hupload{
    static public class Config{

        static public string server{
            get { return Properties.Settings.Default.server; }
            set{ Properties.Settings.Default.server = value; }
        }

        static public int port {
            get { return Properties.Settings.Default.port; }
            set { Properties.Settings.Default.port = value; }
        }

        static public string httpPath {
            get { return Properties.Settings.Default.httpPath; }
            set { Properties.Settings.Default.httpPath = value; }
        }

        static public string username {
            get { return Properties.Settings.Default.username; }
            set { Properties.Settings.Default.username = value; }
        }

        static public string serverPath {
            get { return Properties.Settings.Default.serverPath; }
            set { Properties.Settings.Default.serverPath = value; }
        }

        static public bool autolaunch {
            get { return Properties.Settings.Default.autolaunch; }
            set { SetAutolaunch(value); }
        }

        static private void SetAutolaunch(bool value) {
            if (Properties.Settings.Default.autolaunch == value) return;
            Properties.Settings.Default.autolaunch = value;
            if (value) {
                Program.RegisterAutoLaunch();
            } else {
                Program.UnregisterAutoLaunch();
            }
        }

        static public void Save(){
            Properties.Settings.Default.Save();
        }

        static public bool CheckValid(){
            return true;
        }

    }

}