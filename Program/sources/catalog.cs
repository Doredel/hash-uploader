using System.Collections.Generic;
using System;

namespace hupload{
    public class Catalog{

        Dictionary<string,string> library;
        Dictionary<string,string> cache;

        Server server;

        public int entries => library.Count;
        public Action<int> onUpdate;

        public Catalog(Server server, string[] dict){
            this.server = server;

            library = new Dictionary<string,string>();
            cache = new Dictionary<string,string>();

            foreach(var item in dict){
                string[] entry = item.Split(':');
                if(string.IsNullOrEmpty(entry[0])) continue;
                library.Add(entry[0], entry[1]);
            }
        }

        /// Searches for hash in library
        public string Find(string hash){
            if(!library.ContainsKey(hash)) return "";
            return library[hash];
        }

        /// Stores new hash in library
        public bool Store(string hash, string url){
            if(library.ContainsKey(hash)) return false;
            library.Add(hash, url);
            cache.Add(hash, url);
            return true;
        }

        /// Update server hash table
        public bool Update(){
            var data = new List<string>();
            foreach(var entry in cache){
                data.Add(string.Format("{0}:{1}",entry.Key,entry.Value));
            }

            if(!server.SaveLib(data)) return false;

            cache.Clear();
            onUpdate(entries);
            return true;
        }

    }
}