using System;
using System.IO.Pipes;
using System.Text;

namespace hupload{

    public static class Pipe{

        private static string STOP_WAIT_CMD = ":?stop";

        private static NamedPipeServerStream server;

        public static void StartReading(){
            server = new NamedPipeServerStream("HUPipeServer", PipeDirection.In, 1, PipeTransmissionMode.Message, PipeOptions.Asynchronous);
            server.BeginWaitForConnection(new AsyncCallback(onConnect),server);
        }

        public static void StopReading() {
            var stream = new NamedPipeClientStream(".", "HUPipeServer", PipeDirection.Out, PipeOptions.Asynchronous);
            try { stream.Connect(250); } catch (TimeoutException) { return; }
            byte[] buffer = Encoding.UTF8.GetBytes(STOP_WAIT_CMD);
            stream.BeginWrite(buffer, 0, buffer.Length, new AsyncCallback(onSend), stream);
        }

        private static void onConnect(IAsyncResult result){
            var server = (NamedPipeServerStream)result.AsyncState;
            server.EndWaitForConnection(result);
            byte[] buffer = new byte[255];
            server.Read(buffer,0,255);
            string msg = Encoding.UTF8.GetString(buffer, 0, buffer.Length);

            msg = msg.Replace("\0","");

            if (msg == STOP_WAIT_CMD) {
                _CmdStop();
                return;
            }

            if(!string.IsNullOrEmpty(msg)) Program.Submit(msg);

            server.Close();
            server = null;

            StartReading();
        }

        private static void _CmdStop() {
            server.Close();
            server = null;
        }

        private static void onSend(IAsyncResult result) {
            var stream = (NamedPipeClientStream)result.AsyncState;
            stream.EndWrite(result);
            stream.Flush();
            stream.Close();
            stream.Dispose();
        }

    }

}