namespace hupload{
    public static class Const{
        public const int sizeLimitMB = 50;
        public const int sizeLimit = sizeLimitMB * 1024 * 1024;
    }
}