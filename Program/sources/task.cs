using System;
using System.IO;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;

namespace hupload{
    public class UploadingTask{
        public readonly string filename;
        readonly FileInfo fileInfo;

        string fileHash;
        string fileURL;

        Server server;

        Action onComplete;
        Action onFail;
        List<string> log;

        public UploadingTask(string filename, Server server){
            this.filename = filename;
            log = new List<string>();
            this.server = server;
            
            fileInfo = new FileInfo(filename);
        }

        /// Execute task process
        public void Run(Action onComplete = null, Action onFail = null){
            if(onComplete == null) onComplete = CopyURL;
            this.onComplete = onComplete;

            if(onFail == null) onFail = Cancel;
            this.onFail = onFail;

            _Run();
        }

        private void _Run(){
            
            if(!Check()){
                onFail();
                return;
            }

            if((!server.isConnected && !server.Connect()  ||//server is not connected
               (server.isConnected && !server.TestConnection() && !server.Connect())) ) {//server thinks it is connected, but it is not
                log.Add("Unable to connect to server");
                onFail();
                return;
            }

            fileHash = Utils.calcHash(filename);

            if (fileHash==""){
                log.Add("Could not calculate hash");
                onFail();
                return;
            }

            fileURL = server.catalog.Find(fileHash);

            if (!string.IsNullOrEmpty(fileURL)){
                onComplete();
                return;
            }

            if(!server.Upload(filename, out fileURL, ProcessUpload)){
                log.Add("Could not upload file");
                onFail();
                return;
            }

            if(!server.catalog.Store(fileHash, fileURL)){
                log.Add("Cannot store hash");
                onFail();
                return;
            }

            server.catalog.Update();

            onComplete();
        }

        private void ProcessUpload(ulong progress){
            //Console.WriteLine(String.Format("{0}/{1}",aloe,fileInfo.Length));
        }

        /// Check if file is valid for upload
        public bool Check(){
            long length = fileInfo.Length;
            if (length > Const.sizeLimit) {
                log.Add(string.Format("Files with size more than {0} MB cannot be uploaded",Const.sizeLimitMB));
                return false;
            }
            return true;
        }

        /// Default action for onComplete
        private void CopyURL(){
            server.Disconnect();
            log.Add(string.Format("File available by link {0}", Server.HTTPPath+fileURL));
            var thread = new Thread(()=>Clipboard.SetText(Server.HTTPPath+fileURL));
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();
            ShowLog();
        }

        /// Default action for onFail
        private void Cancel(){
            Program.mainForm.ShowWindow();
            ShowLog();
        }

        private void ShowLog(){
            foreach(var msg in log){
                Program.Log(msg);
            }
        }

    }
}