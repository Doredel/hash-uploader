using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Renci.SshNet;

namespace hupload{
    public class Server :IDisposable{

        bool isDisposed = false;

        string servername;
        int serverport;

        string serverPath;
        const string uploadFolder = "files";
        const string confFolder = "conf";
        const string dataFolder = "data";
        static public string HTTPPath;

        string dataPath;
        string libFileName;

        readonly string username;
        readonly string password;

        SftpClient client;

        public Catalog catalog {
            get;
            private set;
        }

        public bool isConnected => client.IsConnected;

        public Server(string pass){
            servername = Config.server;
            serverport = Config.port;
            serverPath = Config.serverPath;
            HTTPPath = Config.httpPath;

            username = Config.username;
            password = pass;

            dataPath = serverPath + dataFolder;
            libFileName = serverPath + dataFolder + "/hash.lib";

            client = new SftpClient(servername, serverport, this.username, this.password);
        }

        /// Get lines from library
        private string[] GetLib(){
            if (isDisposed) throw new ObjectDisposedException("Server");
            if (!client.Exists(dataPath)) client.CreateDirectory(dataPath);
            if (!client.Exists(libFileName)) client.Create(libFileName);
            
            var data = client.ReadAllLines(libFileName);
            return data;
        }

        /// Save new lines in library
        public bool SaveLib(IEnumerable<string> lines){
            if (isDisposed) throw new ObjectDisposedException("Server");
            client.AppendAllLines(libFileName,lines);
            return true;
        }

        public bool Connect(){
            if (isDisposed) throw new ObjectDisposedException("Server");
            if (client.IsConnected) return true;
            try {
                client.Connect();
            }
            catch (Exception ex){
                Program.Log(ex.Message);
                Program.LogError(ex);
                return false;
            }

            if (!client.IsConnected) return false;

            client.ChangeDirectory(serverPath);
            //@TODO need to check if catalog refresh is really needed
            catalog = new Catalog(this,GetLib());
            Program.mainForm.UpdateFileCount(catalog.entries);
            catalog.onUpdate += Program.mainForm.UpdateFileCount;
            return true;
        }

        public bool TestConnection(){
            if (isDisposed) throw new ObjectDisposedException("Server");
            string bucketPath = serverPath + uploadFolder;
            try {
                var files = client.ListDirectory(bucketPath);
            }
            catch (Exception){
                return false;
            }
            return true;
        }

        public void Disconnect(){
            if (isDisposed) throw new ObjectDisposedException("Server");
            if (!client.IsConnected) return;
            client.Disconnect();
        }

        /// Upload file to server
        public bool Upload(string fullname, out string serverFilename, Action<ulong> callback){
            if (isDisposed) throw new ObjectDisposedException("Server");
            serverFilename = "";

            string bucketPath = serverPath + uploadFolder;
            if (!client.Exists(bucketPath)) client.CreateDirectory(bucketPath);

            var info = new FileInfo(fullname);

            string basename = Path.GetFileNameWithoutExtension(info.Name);
            basename = Regex.Replace(basename,@"\s+","_").Replace(' ','_');

            string extension = Path.GetExtension(info.Name);
            serverFilename = basename+extension;

            int postfix = 0;
            while(client.Exists(bucketPath + serverFilename)){
                if(++postfix>99) return false;
                serverFilename = string.Format("{0}_{1:D2}{2}",basename,postfix,extension);
            }

            using (FileStream file = new FileStream(fullname,FileMode.Open,FileAccess.Read)){
                client.UploadFile(file,bucketPath+'/'+serverFilename,callback);
            }

            if(!client.Exists(bucketPath+'/'+serverFilename)) return false;

            return true;

        }

        public void Dispose() {
            if (isDisposed) return;
            client.Dispose();
            catalog = null;
        }

    }
}